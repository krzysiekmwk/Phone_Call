package pl.knowakowski.phonecall;

import java.io.File;
import java.io.IOException;

import android.media.MediaRecorder;
import android.os.Environment;

class AudioRecorder {

    private MediaRecorder recorder;
    private String path;

    /**
     * Creates a new audio recording at the given path (relative to root of SD card).
     */
    private AudioRecorder(String path) {
        this.path = path;
    }

    AudioRecorder() {
        this(Environment.getExternalStorageDirectory().getAbsolutePath() + "/callRecord.3gp");
    }

    void setPath(String path) {
        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        if (!path.contains(".")) {
            path += ".3gp";
        }
        this.path = path;
    }

    /**
     * Starts a new recording.
     */
    void start() throws IOException {
        String state = android.os.Environment.getExternalStorageState();
        if(!state.equals(android.os.Environment.MEDIA_MOUNTED))  {
            throw new IOException("SD Card is not mounted.  It is " + state + ".");
        }

        // make sure the directory we plan to store the recording in exists
        File directory = new File(path).getParentFile();
        if (!directory.exists() && !directory.mkdirs()) {
            throw new IOException("Path to file could not be created.");
        }

        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(path);
        recorder.prepare();
        recorder.start();
    }

    /**
     * Stops a recording that has been previously started.
     */
    void stop() throws IOException {
        recorder.stop();
        recorder.reset();
        recorder.release();
    }

}