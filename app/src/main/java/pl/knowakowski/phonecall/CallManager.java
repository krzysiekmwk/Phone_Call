package pl.knowakowski.phonecall;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

class CallManager {

    // Shared Preferences
    private SharedPreferences pref;

    private Editor editor;

    // Shared preferences file name
    private static final String PREF_NAME = "PhoneCall";

    private static final String KEY_CON_NUM = "connectionNumber";
    private static final String KEY_DATA_MODIFY = "dateOfModify";

    CallManager(Context context) {
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    void setIndexOfCall(int indexOfCall) {

        editor.putInt(KEY_CON_NUM, indexOfCall);

        // commit changes
        editor.commit();
    }

    int getIndexOfCall(){
        return pref.getInt(KEY_CON_NUM, 0);
    }

    void setKeyDataModify(long indexOfCall) {
        editor.putLong(KEY_DATA_MODIFY, indexOfCall);
        editor.commit();
    }

    long getKeyDataModify(){
        return pref.getLong(KEY_DATA_MODIFY, 0);
    }
}