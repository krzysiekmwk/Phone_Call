package pl.knowakowski.phonecall;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

class ConsultantDataManager {

    // Shared Preferences
    private SharedPreferences pref;

    private Editor editor;

    // Shared preferences file name
    private static final String PREF_NAME = "ConsultantDataManager";

    private static final String KEY_CON_NUM = "name";
    private static final String KEY_EMAIL = "email";

    ConsultantDataManager(Context context) {
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    void setName(String name) {
        editor.putString(KEY_CON_NUM, name);
        editor.commit();
    }

    String getName(){
        return pref.getString(KEY_CON_NUM, "Krystian Policha");
    }

    void setEmail(String email) {
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }

    String getEmail(){
        return pref.getString(KEY_EMAIL, "krystian.policha@otoodszkodowania.pl");
    }
}