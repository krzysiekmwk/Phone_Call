package pl.knowakowski.phonecall;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

class EndCallListener extends PhoneStateListener {
    private boolean ringing = true;

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        String LOG_TAG = "msg";

        if(TelephonyManager.CALL_STATE_RINGING == state) {
            Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
        }
        if(TelephonyManager.CALL_STATE_OFFHOOK == state) {
            //wait for phone to go offhook (probably set a boolean flag) so you know your app initiated the call.
            //connectionStatus = TelephonyManager.CALL_STATE_OFFHOOK;
            ringing = true;
            Log.i(LOG_TAG, "OFFHOOK");
        }
        if(TelephonyManager.CALL_STATE_IDLE == state) {
            //when this state occurs, and your flag is set, restart your app
            ringing = false;
            Log.i(LOG_TAG, "IDLE");
        }
    }

    boolean isRinging() {
        return ringing;
    }

    void setRinging(boolean ringing) {
        this.ringing = ringing;
    }
}