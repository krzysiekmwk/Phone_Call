package pl.knowakowski.phonecall;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private String filePath;
    private String outputAudioRecorderDir = "/records";

    private List<String> numberList;
    private EndCallListener callListener;
    private AudioRecorder audioRecorder;

    private CallManager callManager;
    private ConsultantDataManager consultantDataManager;

    private Context context;

    private TextView actualIndexTextView;
    private TextView swiadomoscKlienta_TextView;

    private boolean isThreadWorking = false;
    private int interval = 10;
    private boolean wasSaved = false;
    private boolean isEndOfList = false;

    private CheckBox deklaracja_checkBox;
    private CheckBox nieZainteresowany_checkBox;
    private CheckBox nieMialSzkody_checkBox;
    private CheckBox autoZastepcze_checkBox;
    private CheckBox szkodaOsobowa_checkBox;
    private CheckBox biznes_checkBox;
    private CheckBox ponownyKontakt_checkBox;

    private RadioButton clientAnsweredNOradioButton;
    private RadioButton clientAnsweredYESradioButton;

    private RadioButton OC_Calkowite_radioButton;
    private RadioButton OC_Czesciowe_radioButton;
    private RadioButton AC_Calkowite_radioButton;
    private RadioButton AC_Czesciowe_radioButton;
    private RadioButton AC_Kradziez_radioButton;
    private RadioButton redColor_radioButton;
    private RadioButton greenColor_radioButton;
    private RadioButton whiteColor_radioButton;
    private RadioButton warsztat_radioButton;
    private RadioButton firmaTransportowa_radioButton;
    private RadioButton inne_radioButton;

    private EditText additionalInfoEditText;
    private EditText imieNazwiskoEditText;
    private EditText numerRejestracyjnyEditText;
    private EditText kosztorysEditText;

    private SeekBar swiadomoscKlientaSeekBar;

    private Spinner carListSpinner;

    private LinearLayout OC_AC_LinearLayout;
    private LinearLayout biznes_LinearLayout;

    private Switch czyWysylacSMSSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (shouldAskPermissions()) {
            askPermissions();
        }

        context = getApplicationContext();
        filePath = Environment.getExternalStorageDirectory() + "/" + getApplicationName(context);
        numberList = new LinkedList<>();

        callManager = new CallManager(context);
        consultantDataManager = new ConsultantDataManager(context);

        audioRecorder = new AudioRecorder();

        actualIndexTextView = (TextView) findViewById(R.id.actualIndexTextView);
        swiadomoscKlienta_TextView = (TextView) findViewById(R.id.swiadomoscKlienta_TextView);

        Button button = (Button) findViewById(R.id.button);
        Button buttonStop = (Button) findViewById(R.id.stop_loop);
        Button buttonSaveData = (Button) findViewById(R.id.save_data);

        deklaracja_checkBox = (CheckBox) findViewById(R.id.deklaracja_checkBox);
        nieZainteresowany_checkBox = (CheckBox) findViewById(R.id.nieZainteresowany_checkBox);
        nieMialSzkody_checkBox = (CheckBox) findViewById(R.id.nieMialSzkody_checkBox);
        autoZastepcze_checkBox = (CheckBox) findViewById(R.id.autoZastepcze_checkBox);
        szkodaOsobowa_checkBox = (CheckBox) findViewById(R.id.szkodaOsobowa_checkBox);
        biznes_checkBox = (CheckBox) findViewById(R.id.biznes_checkBox);
        ponownyKontakt_checkBox = (CheckBox) findViewById(R.id.ponownyKontakt_checkBox);

        clientAnsweredNOradioButton = (RadioButton) findViewById(R.id.clientAnsweredNO);
        clientAnsweredYESradioButton = (RadioButton) findViewById(R.id.clientAnsweredYES);

        OC_Calkowite_radioButton = (RadioButton) findViewById(R.id.OC_Calkowite_radioButton);
        OC_Czesciowe_radioButton = (RadioButton) findViewById(R.id.OC_Czesciowe_radioButton);
        AC_Calkowite_radioButton = (RadioButton) findViewById(R.id.AC_Calkowite_radioButton);
        AC_Czesciowe_radioButton = (RadioButton) findViewById(R.id.AC_Czesciowe_radioButton);
        AC_Kradziez_radioButton = (RadioButton) findViewById(R.id.AC_Kradziez_radioButton);
        redColor_radioButton = (RadioButton) findViewById(R.id.redColor_radioButton);
        greenColor_radioButton = (RadioButton) findViewById(R.id.greenColor_radioButton);
        whiteColor_radioButton = (RadioButton) findViewById(R.id.whiteColor_radioButton);
        warsztat_radioButton = (RadioButton) findViewById(R.id.warsztat_radioButton);
        firmaTransportowa_radioButton = (RadioButton) findViewById(R.id.firmaTransportowa_radioButton);
        inne_radioButton = (RadioButton) findViewById(R.id.inne_radioButton);

        additionalInfoEditText = (EditText) findViewById(R.id.additionalInfoEditText);
        imieNazwiskoEditText = (EditText) findViewById(R.id.imie_nazwisko_EditText);
        numerRejestracyjnyEditText = (EditText) findViewById(R.id.numerRejestracyjny_EditText);
        kosztorysEditText = (EditText) findViewById(R.id.kosztorys_EditText);

        swiadomoscKlientaSeekBar = (SeekBar) findViewById(R.id.swiadomoscKlienta_seekBar);

        carListSpinner = (Spinner) findViewById(R.id.carList_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.car_array, android.R.layout.simple_spinner_item);
        // Apply the adapter to the spinner
        carListSpinner.setAdapter(adapter);

        OC_AC_LinearLayout = (LinearLayout) findViewById(R.id.OC_AC_LinearLayout) ;
        biznes_LinearLayout = (LinearLayout) findViewById(R.id.biznes_LinearLayout);

        czyWysylacSMSSwitch = (Switch) findViewById(R.id.wysylanieSMS_switch);

        makeDirectory();
        fillListNumbersFromFile(numberList);

        TextView listSizeTextView = (TextView) findViewById(R.id.listSizeTextView);
        listSizeTextView.setText(Integer.toString(numberList.size()));
        actualIndexTextView.setText("-");

        Log.d("size", String.valueOf(numberList.size()));
        for (String nr : numberList) {
            Log.d("nr", nr);
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((callManager.getIndexOfCall() != 0) && !isEndOfList && !wasSaved)
                    saveData();
                wasSaved = false;
                interval = 10;
                startThreadCalling();
                saveLooper(true);
            }
        });
        buttonSaveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!wasSaved)
                    saveData();
                else
                    showTextInToast("Nacisnij START");
                wasSaved = true;
                interval = 0;
            }
        });
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isThreadWorking) {
                    wasSaved = true;
                    isThreadWorking = false;
                }
                interval = 0;
                showTextInToast("Wtrzymano automat do dzwonienia. Przerwa.");
                saveLooper(false);
            }
        });

        clientAnsweredYESradioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interval = 30;
            }
        });

        deklaracja_checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clientAnsweredYESradioButton.setChecked(true);
                if(isThreadWorking) {
                    wasSaved = true;
                    isThreadWorking = false;
                }
                interval = 0;
                showTextInToast("Wtrzymano automat do dzwonienia");
                saveLooper(false);

                OC_AC_LinearLayout.setVisibility(View.VISIBLE);
                redColor_radioButton.setChecked(true);
                OC_Calkowite_radioButton.setChecked(true);
            }
        });

        biznes_checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                biznes_LinearLayout.setVisibility(View.VISIBLE);
                inne_radioButton.setChecked(true);
            }
        });

        swiadomoscKlientaSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                swiadomoscKlienta_TextView.setText(Integer.toString(swiadomoscKlientaSeekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        callListener = new EndCallListener();
        TelephonyManager mTM = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        mTM.listen(callListener, PhoneStateListener.LISTEN_CALL_STATE);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isThreadWorking = false;
    }

    private void sendSMS(String phoneNumber) {
        String message = getString(R.string.sms_NIE_MIAL_SZKODY);
        if(OC_Calkowite_radioButton.isChecked())
            message = getString(R.string.sms_OC_CALK);

        else if(OC_Czesciowe_radioButton.isChecked())
            message = getString(R.string.sms_OC_CZESC);

        else if(AC_Calkowite_radioButton.isChecked())
            message = getString(R.string.sms_AC_CALK);

        else if(AC_Czesciowe_radioButton.isChecked())
            message = getString(R.string.sms_AC_CZESC);

        else if(AC_Kradziez_radioButton.isChecked())
            message = getString(R.string.sms_AC_KRADZIEZ);

        else if(autoZastepcze_checkBox.isChecked())
            message = getString(R.string.sms_AUTO_ZASTEPCZE);

        else if(firmaTransportowa_radioButton.isChecked())
            message = getString(R.string.sms_BIZNES_FIRMA_TRANSPORTOWA);

        else if(inne_radioButton.isChecked())
            message = getString(R.string.sms_BIZNES_INNY);

        else if(warsztat_radioButton.isChecked())
            message = getString(R.string.sms_BIZNES_WARSZTAT);

        else if(szkodaOsobowa_checkBox.isChecked())
            message = getString(R.string.sms_SZKODA_OSOBOWA);

        else if(nieZainteresowany_checkBox.isChecked())
            message = getString(R.string.sms_MIAL_SZKODE_NIE_ZAINTERESOWANY);

        else if(nieMialSzkody_checkBox.isChecked())
            message = getString(R.string.sms_NIE_MIAL_SZKODY);

        else if(ponownyKontakt_checkBox.isChecked())
            message = getString(R.string.sms_PONOWNY_KONTAKT);

        message = addConsultantNameAndEmail(message);

        //if(clientAnsweredYESradioButton.isChecked() && !ponownyKontakt_checkBox.isChecked()) {
        if(clientAnsweredYESradioButton.isChecked()) {
            SmsManager sms = SmsManager.getDefault();
            ArrayList<String> mSMSMessage = sms.divideMessage(message);
            sms.sendMultipartTextMessage(phoneNumber, null, mSMSMessage, null, null);
        }
    }

    private String addConsultantNameAndEmail(String message){
        message = message.replaceAll("#email#", consultantDataManager.getEmail());
        message = message.replaceAll("#name#", consultantDataManager.getName());
        return message;
    }

    private void saveData(){
        if(!callListener.isRinging()) {
            if (clientAnsweredNOradioButton.isChecked()) {
                System.out.println((callManager.getIndexOfCall() - 1));
                if ((callManager.getIndexOfCall() - 1) < 0)
                    showTextInToast("Powinieneś napierw naciasnac START");
                else
                    saveMissedCallNumber(numberList.get(callManager.getIndexOfCall() - 1));
            }
            else {
                StringBuilder sb = new StringBuilder();
                sb.append(" DEKLARACJA: ").append(deklaracja_checkBox.isChecked());
                sb.append(" OC CALKOWITE: ").append(OC_Calkowite_radioButton.isChecked());
                sb.append(" OC CZESCIOWE: ").append(OC_Czesciowe_radioButton.isChecked());
                sb.append(" AC CALKOWITE: ").append(AC_Calkowite_radioButton.isChecked());
                sb.append(" AC CZESCIOWE: ").append(AC_Czesciowe_radioButton.isChecked());
                sb.append(" AC KRADZIEZ: ").append(AC_Kradziez_radioButton.isChecked());
                sb.append(" CZERWONY: ").append(redColor_radioButton.isChecked());
                sb.append(" ZIELONY: ").append(greenColor_radioButton.isChecked());
                sb.append(" BIALY: ").append(whiteColor_radioButton.isChecked());
                sb.append(" MIAL SZKODE/NIE ZAINTERESOWANY: ").append(nieZainteresowany_checkBox.isChecked());
                sb.append(" NIE MIAL SZKODY: ").append(nieMialSzkody_checkBox.isChecked());
                sb.append(" AUTO ZASTEPCZE: ").append(autoZastepcze_checkBox.isChecked());
                sb.append(" SZKODA OSOBOWA: ").append(szkodaOsobowa_checkBox.isChecked());
                sb.append(" BIZNES: ").append(biznes_checkBox.isChecked());
                sb.append(" WARSZTAT: ").append(warsztat_radioButton.isChecked());
                sb.append(" FIRMA TRANSPORTOWA: ").append(firmaTransportowa_radioButton.isChecked());
                sb.append(" INNE: ").append(inne_radioButton.isChecked());
                sb.append(" PONOWNY KONTAKT: ").append(ponownyKontakt_checkBox.isChecked());
                sb.append(" DODATKOWA INFORMACJA: ").append(additionalInfoEditText.getText().toString());

                sb.append(" IMIĘ I NAZWISKO: ").append(imieNazwiskoEditText.getText().toString());
                sb.append(" NUMER REJESTRACYJNY: ").append(numerRejestracyjnyEditText.getText().toString());
                sb.append(" KOSZTORYS: ").append(kosztorysEditText.getText().toString());
                sb.append(" MARKA: ").append(carListSpinner.getSelectedItem().toString());
                sb.append(" SWIADOMOSC KLIENTA: ").append(Integer.toString(swiadomoscKlientaSeekBar.getProgress()));

                String str = sb.toString();

                saveCallInformation(lastCallInformation() + " " + str + "\r\n");
            }

            if(ponownyKontakt_checkBox.isChecked()){
                saveMissedCallNumber(numberList.get(callManager.getIndexOfCall() - 1));
            }

            showTextInToast("Zapisano!");
            if(czyWysylacSMSSwitch.isChecked()) {
                sendSMS(numberList.get(callManager.getIndexOfCall() - 1));
            }
            clearAllInterface();
        }
        else {
            showTextInToast("Prosze najpierw zakonczyc rozmowe");
        }
    }

    private void clearAllInterface(){
        runOnUiThread(new Runnable() {
            public void run()
            {
                deklaracja_checkBox.setChecked(false);
                nieZainteresowany_checkBox.setChecked(false);
                nieMialSzkody_checkBox.setChecked(false);
                autoZastepcze_checkBox.setChecked(false);
                szkodaOsobowa_checkBox.setChecked(false);
                biznes_checkBox.setChecked(false);
                ponownyKontakt_checkBox.setChecked(false);

                OC_Calkowite_radioButton.setChecked(false);
                OC_Czesciowe_radioButton.setChecked(false);
                AC_Calkowite_radioButton.setChecked(false);
                AC_Czesciowe_radioButton.setChecked(false);
                AC_Kradziez_radioButton.setChecked(false);
                redColor_radioButton.setChecked(false);
                greenColor_radioButton.setChecked(false);
                whiteColor_radioButton.setChecked(false);
                warsztat_radioButton.setChecked(false);
                firmaTransportowa_radioButton.setChecked(false);
                inne_radioButton.setChecked(false);

                clientAnsweredNOradioButton.setChecked(true);

                additionalInfoEditText.setText("");
                imieNazwiskoEditText.setText("");
                numerRejestracyjnyEditText.setText("");
                kosztorysEditText.setText("");

                swiadomoscKlientaSeekBar.setProgress(1);

                carListSpinner.setSelection(0,true);

                OC_AC_LinearLayout.setVisibility(View.GONE);
                biznes_LinearLayout.setVisibility(View.GONE);
            }
        });

    }

    private void startThreadCalling() {
        if(!isThreadWorking) {
            isThreadWorking = true;

            new Thread(new Runnable() {
                public void run() {
                    // a potentially  time consuming task
                    while (isThreadWorking) {
                        if (callManager.getIndexOfCall() < numberList.size()) {
                            setActualIndexNumberTextView(callManager.getIndexOfCall() + 1);
                            String number = numberList.get(callManager.getIndexOfCall());
                            Log.d("Make Call: ", number);
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", number, null));
                            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                                startActivity(intent);

                                callListener.setRinging(true);
                                System.out.println("BEFORE");

                                try {
                                    Date date = Calendar.getInstance().getTime();
                                    SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH-mm-ss");
                                    String dateString = formatter.format(date);

                                    audioRecorder.setPath(filePath + outputAudioRecorderDir + "/" + number + "_" + dateString);
                                    audioRecorder.start();
                                    while (callListener.isRinging()) {
                                        //Phone is calling
                                    }
                                    System.out.println("afterre");
                                    audioRecorder.stop();

                                    Thread.sleep(500);

                                    callManager.setIndexOfCall(callManager.getIndexOfCall() + 1);

                                    for (int i = 0; i < interval; i++) {
                                        Thread.sleep(1000);
                                    }

                                    if (!wasSaved)
                                        saveData();

                                    wasSaved = false;
                                    interval = 10;

                                    Log.e("fin", "end of calling procedure");
                                } catch (IOException | InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            isThreadWorking = false;
                            isEndOfList = true;
                            showTextInToast("Nie ma więcej numerów na liście");
                        }
                    }
                }
            }).start();
            Log.e("fin","end of thread");
        }
    }

    public String lastCallInformation() {
        StringBuilder sb = new StringBuilder();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            return "";
        }
        Cursor cur = getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, android.provider.CallLog.Calls.DATE + " DESC limit 1;");

        int number = cur.getColumnIndex( CallLog.Calls.NUMBER );
        int date = cur.getColumnIndex(CallLog.Calls.DATE);
        int duration = cur.getColumnIndex( CallLog.Calls.DURATION);
        sb.append("Call Details:");
        while ( cur.moveToNext() ) {
            String phNumber = cur.getString( number );
            String callDate = cur.getString( date );
            String callDuration = cur.getString( duration );
            sb.append(" Phone Number: ").append(phNumber);
            sb.append(" Duration: ").append(callDuration).append("s");

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String dateString = formatter.format(new Date(Long.parseLong(callDate)));

            sb.append(" Date: ").append(dateString);
            break;
        }
        cur.close();
        return sb.toString();
    }

    private void setActualIndexNumberTextView(final int actualIndex){
        runOnUiThread(new Runnable() {
            public void run()
            {
                actualIndexTextView.setText(Integer.toString(actualIndex));
            }
        });
    }

    private void showTextInToast(final String text){
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText(context,text,Toast.LENGTH_LONG).show();
            }
        });
    }

    private void saveCallInformation(String information){
        //Append on the end of file all data
        System.out.println(information);

        String outputFileName = "/saved_info.txt";
        File file = new File(filePath + outputFileName);
        try {
            if(file.createNewFile())
                System.out.println("Created new file to save data");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileOutputStream fos = new FileOutputStream (new File(file.getAbsolutePath()), true);

            fos.write(information.getBytes());
            fos.flush();
            fos.close();
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveLooper(boolean open){
        String outputLoopersInfoFileName = "/loopers_info.txt";
        File file = new File(filePath + outputLoopersInfoFileName);
        try {
            if(file.createNewFile())
                System.out.println("Created new file to save data");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        String dateString = formatter.format(date);

        try {
            FileOutputStream fos = new FileOutputStream (new File(file.getAbsolutePath()), true);

            if(open)
                fos.write((dateString + " Uruchomiono pętlę." + "\r\n").getBytes());
            else
                fos.write((dateString + " Zatrzymano pętlę." + "\r\n").getBytes());
            fos.flush();
            fos.close();
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveMissedCallNumber(String number){
        String outputMissedCallNumberFileName = "/saved_numbers.txt";
        File file = new File(filePath + outputMissedCallNumberFileName);
        try {
            if(file.createNewFile())
                System.out.println("Created new file to save data");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileOutputStream fos = new FileOutputStream (new File(file.getAbsolutePath()), true);

            fos.write((number + "\r\n").getBytes());
            fos.flush();
            fos.close();
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fillListNumbersFromFile(List<String> numberList){
        String inputFileName = "/numbers.txt";
        File file = new File(filePath + inputFileName);
        try {
            if(file.createNewFile())
                System.out.println("Created new file");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileInputStream fis = new FileInputStream(file);

            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);

            String line;
            try {
                while ((line = br.readLine()) != null){
                    numberList.add(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(callManager.getKeyDataModify() != file.lastModified()){
                callManager.setIndexOfCall(0);
                callManager.setKeyDataModify(file.lastModified());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void makeDirectory(){
        File fileDir = new File(filePath);
        if(fileDir.mkdirs())
            System.out.println("Created new directory");
    }

    private static String getApplicationName(Context context) {
        return (String) context.getApplicationInfo().loadLabel(context.getPackageManager());
    }

    public void setClientAnsweredYES(View view) {
        clientAnsweredYESradioButton.setChecked(true);
        interval = 30;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.settings:
                Intent intent = new Intent(MainActivity.this, SettingConsultantDataActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected boolean shouldAskPermissions() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(23)
    protected void askPermissions() {
        while(checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {
                    "android.permission.READ_EXTERNAL_STORAGE",
                    "android.permission.WRITE_EXTERNAL_STORAGE",
                    "android.permission.CALL_PHONE",
                    "android.permission.READ_CALL_LOG",
                    "android.permission.READ_EXTERNAL_STORAGE",
                    "android.permission.WRITE_EXTERNAL_STORAGE",
                    "android.permission.READ_PHONE_STATE",
                    "android.permission.CAPTURE_AUDIO_OUTPUT",
                    "android.permission.RECORD_AUDIO",
                    "android.permission.SEND_SMS"
            };
            int requestCode = 200;
            requestPermissions(permissions, requestCode);
        }
    }
}
