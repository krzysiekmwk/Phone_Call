package pl.knowakowski.phonecall;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingConsultantDataActivity extends AppCompatActivity {

    private EditText imieNazwiskoEditText;
    private EditText emailEditText;
    private Button zapiszDaneButton;

    private ConsultantDataManager consultantDataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_consultant_data);

        imieNazwiskoEditText = (EditText) findViewById(R.id.imie_nazwisko_EditText);
        emailEditText = (EditText) findViewById(R.id.email_EditText);

        zapiszDaneButton = (Button) findViewById(R.id.zapiszDane_Button);

        consultantDataManager = new ConsultantDataManager(getApplicationContext());

        imieNazwiskoEditText.setText(consultantDataManager.getName());
        emailEditText.setText(consultantDataManager.getEmail());

        zapiszDaneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                consultantDataManager.setName(imieNazwiskoEditText.getText().toString());
                consultantDataManager.setEmail(emailEditText.getText().toString());
                Toast.makeText(getApplicationContext(),"Zapisano!",Toast.LENGTH_LONG).show();
            }
        });
    }
}
